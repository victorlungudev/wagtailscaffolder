import subprocess
import jinja2
import argparse
from collections import OrderedDict
from xlrd import open_workbook
from pyexcel_ods import get_data


class Rederer:
    def __init__(self, template):
        with open(template, 'r') as tpl:
            self.template = jinja2.Template(tpl.read())

    def render(self, context):
        return self.template.render(context)


class DataReader:
    def __init__(self, filename):
        self.filename = filename

    def get_data(self):
        raise NotImplementedError


class XlsDataReader(DataReader):
    def get_data(self):
        return get_data(self.filename)


class OdsDataReader(DataReader):

    def _get_row_data(self, sheet):
        num_rows, num_cols = sheet.nrows, sheet.ncols
        result = []
        for current_row in range(0, num_rows, 1):
            row_data = []
            for current_col in range(0, num_cols, 1):
                data = sheet.cell_value(current_row, current_col)
                row_data.append(data)
            result.append(row_data)
        return result

    def get_data(self):
        data = OrderedDict(dict())
        workbook = open_workbook(self.filename)
        sheets = workbook.sheets()
        for sheet in sheets:
            _sheet = workbook.sheet_by_name(sheet)
            row_data = self._get_row_data(_sheet)
            data.update({_sheet: row_data})
        return data


class WagtailFieldsValidator:
    fields = ['RichTextField',
              'StreamField']


class DjangoModelFieldsValidator:
    fields = ['AutoField',
              'BigAutoField',
              'BigIntegerField',
              'BinaryField',
              'BooleanField',
              'CharField',
              'DateField',
              'DateTimeField',
              'DecimalField',
              'DurationField',
              'EmailField',
              'FileField',
              'FileField and FieldFile',
              'FilePathField',
              'FloatField',
              'ImageField',
              'IntegerField',
              'GenericIPAddressField',
              'NullBooleanField',
              'PositiveIntegerField',
              'PositiveSmallIntegerField',
              'SlugField',
              'SmallIntegerField',
              'TextField',
              'TimeField',
              'URLField',
              'UUIDField', ]

class Validator:
    fields = WagtailFieldsValidator.fields + DjangoModelFieldsValidator.fields

    @staticmethod
    def validate(data):
        for field in data:
            assert field[1] in Validator.fields, 'Not a valid field name {field}.'.format(field=field)

class DataToContextConverter:
    def __init__(self, raw_data):
        self.raw_data = raw_data

    def to_context(self):
        context = {
            'django_fields': {},
            'wagtail_fields': {}
        }

        for sheet, rows in self.raw_data.items():
            context['page_name'] = sheet
            for row in rows:
                if row[1] in DjangoModelFieldsValidator.fields:
                    context['django_fields'][row[0]] = row[1]
                if row[1] in WagtailFieldsValidator.fields:
                    context['wagtail_fields'][row[0]] = row[1]
        return context



def render_templates(destination_filename, data):
    with open(destination_filename, 'a') as dest_file:
        for sheet, rows in data.items():
            Validator.validate(rows)
            context = DataToContextConverter({sheet: rows}).to_context()
            rendered_template = renderer.render(context)
            print(rendered_template)
            dest_file.write(rendered_template)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("source", help="Source page models file.", type=str)
    parser.add_argument("destination", help="Destination page model file.", type=str)
    parser.add_argument("project_name", help="Wagtail project name", type=str)
    parser.add_argument("template", help="Page model template", type=str)
    parser.add_argument("--format", default='xls', choices=['xls', 'xlsx', 'ods'], type=str, help="File format")
    arguments = parser.parse_args()

    reader = None
    if arguments.format in ['xls', 'xlsx']:
        reader = XlsDataReader(arguments.source)
    if arguments.format in ['ods']:
        reader = OdsDataReader(arguments.source)

    renderer = Rederer(arguments.template)

    data = reader.get_data()

    subprocess.call(["wagtail", "start", arguments.project_name])

    render_templates(arguments.destination, data)



