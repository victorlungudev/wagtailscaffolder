### RUN
```python
    make setup
    python wagtail_boostrap.py SOURCEFILE DESTINATIONFILE PROJECT_NAME TEMPLATE
```

### Improvements
    - Add validation when creating context so to not loop multiple times.
    - Resolve identation issues with jinja template