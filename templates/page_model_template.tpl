class {{page_name}}Page(Page):

    {% for field_name, field_value in django_fields.items() %}
    {{field_name}}=models.{{field_value}}()
    {% endfor %}

	{% for field_name, field_value in wagtail_fields.items() %}
    {{field_name}}={{field_value}}()
    {% endfor %}
